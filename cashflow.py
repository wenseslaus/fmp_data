# To view apple's cash flow

import requests
import json
import pandas as pd

data = requests.get(f'https://financialmodelingprep.com/api/v3/cash-flow-statement/AAPL?apikey=7dccea40db4087b6e17613f508f427e0').json()
df = pd.DataFrame(data)
df.to_csv('cash_flow.csv', index=False)
