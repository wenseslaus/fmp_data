# To view apple's historical data

import requests
import json
import pandas as pd

data = requests.get(f'https://financialmodelingprep.com/api/v3/historical-price-full/AAPL?apikey=7dccea40db4087b6e17613f508f427e0').json()
df = pd.DataFrame(data)
df.to_csv('stocks.csv', index=False)
