# To view apple's balance sheet

import requests
import json
import pandas as pd

data = requests.get(f'https://financialmodelingprep.com/api/v3/balance-sheet-statement/AAPL?apikey=7dccea40db4087b6e17613f508f427e0').json()
df = pd.DataFrame(data)
df.to_csv('balance_sheet.csv', index=False)
