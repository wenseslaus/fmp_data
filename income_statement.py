# To view apple's income statement

import requests
import json
import pandas as pd

data = requests.get(f'https://financialmodelingprep.com/api/v3/income-statement/AAPL?apikey=7dccea40db4087b6e17613f508f427e0').json()
df = pd.DataFrame(data)
df.to_csv('income_stat.csv', index=False)

